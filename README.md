# HHH 16927 Bug Reproduction

See the [associated JIRA Ticket]() for more info.

The **v5 folder** contains the "working" demo (returns BigDecimal instances);

the **v6 folder** demontrates the new behavior (result set contains `Long`s and `Integer`s)
